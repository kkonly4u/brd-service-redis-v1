package com.belk.brd.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.text.StringEscapeUtils;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.belk.brd.model.BrdRequest;
import com.belk.brd.service.BrdService;
import com.belk.brd.util.BrdRspUtil;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class BrdServiceController {

	private JSONArray responseJsonArray;
	private final BrdService service;
	private final BrdRspUtil brdRspUtil;

	@Autowired
	public BrdServiceController(BrdService service, BrdRspUtil brdRspUtil) {
		this.brdRspUtil = brdRspUtil;
		this.service = service;
	}
	
	@PostMapping(path = "/brd")
	public ResponseEntity<String> validateBRDNumber(@RequestBody List<BrdRequest> brdRequest,
			HttpServletRequest request) {

		if (null != brdRequest && brdRequest.size() == 0) {
			log.info("Val " + "Everything null");
			responseJsonArray = brdRspUtil.setEmptyRspForFullInvalidRq();
		}

		if (null != brdRequest && brdRequest.size() > 0 && brdRspUtil.isRqValid(brdRequest.get(0))) {
			responseJsonArray = service.processBRD(brdRequest.get(0),
					/* request.getUserPrincipal().getName() */ "example-client");
		} else {
			responseJsonArray = brdRspUtil.setEmptyRspForInvalidRq(brdRequest.get(0));
		}

		if (responseJsonArray.length() == 0) {
			responseJsonArray = brdRspUtil.setEmptyResponse(brdRequest.get(0));
		}

		String body = StringEscapeUtils.unescapeJson(responseJsonArray.toString());
		log.info("Response: " + body);
		return new ResponseEntity<String>(body, HttpStatus.OK);
	}
	
	@GetMapping("/")
	public String testApi() {
		System.out.println("I am from Docker Image");
		return "I am From Docker Image";
	}
}
