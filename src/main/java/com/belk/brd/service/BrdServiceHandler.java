package com.belk.brd.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

//import org.json.JSONArray;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.belk.brd.model.Attributes;
import com.belk.brd.model.Brd;
import com.belk.brd.model.BrdModel;
import com.belk.brd.model.BrdRequest;
import com.belk.brd.model.BrdRequestArray;
import com.belk.brd.model.BrdResponse;
import com.belk.brd.model.BrdSaveResponse;
import com.belk.brd.repo.BrdRedisRepo;
import com.belk.brd.util.BrdAttributesUtil;
import com.belk.brd.util.BrdRspUtil;
import com.belk.brd.util.BrdUtility;

import io.lettuce.core.api.sync.RedisCommands;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BrdServiceHandler {
	@Autowired
	public BrdModel brdData;
	@Autowired
	public Brd brd;
	@Autowired
	public Attributes attributes;
	@Autowired
	BrdRequest brdRequest;
	@Autowired
	BrdResponse brdResponse;
	@Autowired
	BrdRedisRepo brdRepository;
	@Autowired
	BrdSaveResponse brdSaveResponse;
	@Autowired
	BrdUtility brdUtility;
	@Autowired
	BrdRspUtil brdRspUtil;
	@Autowired
	BrdAttributesUtil brdAttributesUtil;
	
	@Autowired
	private RedisCommands<String, String> syncCommands;
	
	@SuppressWarnings({})
	public JSONArray inquiry_90(BrdRequest brdRequest) {
		// TODO: Remove tight coupling and create utility for looping using JDk1.8
		String numberEntries = (null != brdRequest.getNumberOfEntries() && !brdRequest.getNumberOfEntries().isEmpty()
				? brdRequest.getNumberOfEntries()
				: "0");
		Integer numberOfEntries = Integer.parseInt(numberEntries);
		BrdRequestArray[] brdArray = brdRequest.getBrdArray();
		List<String> brdnumbers = new ArrayList<>();
		List<String> invalidBrdnumbers = new ArrayList<>();

		for (int i = 0; i < brdArray.length; i++) {
			String brdnumber = brdArray[i].getBrdNbr().toString().trim();
			if (brdnumber.length() == 15) {
				brdnumbers.add(brdnumber);
			} else {
				invalidBrdnumbers.add(brdnumber);
			}
		}

		log.info(" 90 validBrdnumbers Size  " + brdnumbers.size());
		log.info(" 90 invalidBrdnumbers Size  " + invalidBrdnumbers.size());
		
//		String queryString = brdUtility.getQuery(brdnumbers);
//		List<BrdModel> data = brdUtility.getDataFromPCC(queryString);
		
		List<BrdModel> data = findBrdsByBrdNbrs(brdnumbers);
		
		
		log.info(" Resultset Size  " + data.size());
		
		// Integer numberOfEntries = data.size() + invalidBrdnumbers.size();
		JSONArray rootReponseArray = new JSONArray();
		JSONArray listBrdArray = new JSONArray();
		if (data.size() > 0) {
			log.info(" Resultset Loop  ");
			rootReponseArray = brdRspUtil.addTypOrdrStore(rootReponseArray, brdRequest);
			for (int i = 0; i < data.size(); i++) {
				JSONObject obj = new JSONObject();
				obj.put("brdNbr", data.get(i).getBrd().getBrdnumber());
				obj = setResponseCode(data, obj, i, "90");
				listBrdArray.put(obj);
			}

		}
		if (data.size() == 0 && brdnumbers.size() == 0) {
			log.info(" setEmptyResponse  ");
			rootReponseArray = brdRspUtil.setEmptyResponse(brdRequest);
		} else if (brdArray.length > 0 && data.size() == 0 && brdnumbers.size() != 0) {
			log.info(" brdArray.length > 0 && data.size() == 0 && brdnumbers.size() != 0");
			rootReponseArray = brdRspUtil.setNoDataFoundResponse(rootReponseArray, brdRequest, brdnumbers, listBrdArray,
					invalidBrdnumbers, numberOfEntries, brdArray);

		} else {
			rootReponseArray = returnValidatedRespondCode(rootReponseArray, brdRequest, brdnumbers, listBrdArray,
					invalidBrdnumbers, numberOfEntries);
		}
		return rootReponseArray;
	}

	public JSONArray returnValidatedRespondCode(JSONArray rootReponseArray, BrdRequest brdRequest,
			List<String> brdnumbers, JSONArray listBrdArray, List<String> invalidBrdnumbers, int numberOfEntries) {

		// complte input BRD list,
		// if input brdnumberslist > numberOfEntries, means missing brd which are 15
		// digit but did not get response from PCC. So build that list of remaining
		// ones.
		// BRDnumbers size >resultSetArray (we sent 5 in query and return only 3)

		// Non Identified
		List<String> nonIdentifyInPCC = new ArrayList<>();
		log.info("IN  " + brdnumbers.size() + " " + listBrdArray.length());
		if (brdnumbers.size() >= listBrdArray.length()) {
			log.info("Satisfied   ");
			// for (int cnt = 0; cnt < brdnumbers.size(); cnt++)
			{
				for (int iCnt = 0; iCnt < listBrdArray.length(); iCnt++) {
					if (brdnumbers.contains(((((JSONObject) listBrdArray.get(iCnt)).get("brdNbr").toString())))) {
						nonIdentifyInPCC.add((((JSONObject) listBrdArray.get(iCnt)).get("brdNbr").toString()));
					}
				}
			}
		}
		brdnumbers.removeAll(nonIdentifyInPCC);
		log.info("Removed  " + brdnumbers.size());
		for (String remainingBRDs : brdnumbers) {
			log.info("gotcha " + remainingBRDs);
			JSONObject inavlidObj = new JSONObject();
			inavlidObj.put("brdNbr", remainingBRDs);
			inavlidObj.put("brdAmt", "0.00");
			inavlidObj.put("brdResp", "001");
			listBrdArray.put(inavlidObj);
		}

		log.info(" Now Invalid Numbers ");
		// Last, add Invalid BRD"s to list as it is.
		if (invalidBrdnumbers.size() > 0) {
			for (int invalid = 0; invalid < invalidBrdnumbers.size(); invalid++) {
				log.info(" invalidBrdnumbers.size() " + invalidBrdnumbers.size());
				JSONObject inavlidObj = new JSONObject();
				inavlidObj.put("brdNbr", invalidBrdnumbers.get(invalid));
				inavlidObj.put("brdAmt", "0.00");
				inavlidObj.put("brdResp", "001");
				listBrdArray.put(inavlidObj);
			}
		}

		// Now we need to filter result based on number of entries.
		// Filter Start
		BrdRequestArray[] inputBrdArray = brdRequest.getBrdArray();
		if (numberOfEntries < inputBrdArray.length) {
			JSONArray finallistBrdArray = new JSONArray();
			log.info(" numberOfEntries < inputBrdArray.length");
			for (int cnt = 0; cnt < numberOfEntries; cnt++) {
				for (int iCnt = 0; iCnt < listBrdArray.length(); iCnt++) {
					if (inputBrdArray[cnt].getBrdNbr().toString().trim()
							.equalsIgnoreCase((((JSONObject) listBrdArray.get(iCnt)).get("brdNbr").toString()))) {
						finallistBrdArray.put((JSONObject) listBrdArray.get(iCnt));
					}
				}
			}
			rootReponseArray.put(finallistBrdArray);
			return rootReponseArray;// Its new array so return only response which is being asked by numEntries
		} else if (numberOfEntries > inputBrdArray.length) {
			// Add blank elements to listBrdArray
			for (int cnt = inputBrdArray.length; cnt < numberOfEntries; cnt++) {
				log.info(" MY Extra cnt " + cnt);
				JSONObject extraObj = new JSONObject();
				extraObj = brdRspUtil.createEmptyBrdObj(extraObj);
				listBrdArray.put(extraObj);
			}
		}
		// Filter END
		rootReponseArray.put(listBrdArray);
		return rootReponseArray;
	}

	@SuppressWarnings({})
	public JSONArray reversal_93(BrdRequest brdRequest, String clientName) {
		// TODO: Remove tight coupling and create utility for looping using JDk1.8
		String numberEntries = (null != brdRequest.getNumberOfEntries() && !brdRequest.getNumberOfEntries().isEmpty()
				? brdRequest.getNumberOfEntries()
				: "0");
		Integer numberOfEntries = Integer.parseInt(numberEntries);
		BrdRequestArray[] brdArray = brdRequest.getBrdArray();
		List<String> brdnumbers = new ArrayList<>();
		List<String> invalidBrdnumbers = new ArrayList<>();
		for (int i = 0; i < brdArray.length; i++) {
			String brdnumber = brdArray[i].getBrdNbr().toString().trim();
			if (brdnumber.length() == 15) {
				brdnumbers.add(brdnumber);
			} else {
				invalidBrdnumbers.add(brdnumber);
			}
		}

//		String queryString = brdUtility.getQuery(brdnumbers);
//		List<BrdModel> data = brdUtility.getDataFromPCC(queryString);
		
		List<BrdModel> data = findBrdsByBrdNbrs(brdnumbers);

		JSONArray rootReponseArray = new JSONArray();
		JSONArray listBrdArray = new JSONArray();
		if (data.size() > 0) {
			rootReponseArray = brdRspUtil.addTypOrdrStore(rootReponseArray, brdRequest);
			// rootReponseArray.add(Integer.toString(data.size()));
			for (int i = 0; i < data.size(); i++) {
				JSONObject obj = new JSONObject();
				obj.put("brdNbr", data.get(i).getBrd().getBrdnumber());
				obj = setReversalResponseCode(brdRequest, data, obj, i, "93", clientName);
				listBrdArray.put(obj);
			}
		}
		if (data.size() == 0 && brdnumbers.size() == 0) {
			log.info(" setEmptyResponse  ");
			rootReponseArray = brdRspUtil.setEmptyResponse(brdRequest);
		} else if (brdArray.length > 0 && data.size() == 0 && brdnumbers.size() != 0) {
			log.info(" brdArray.length > 0 && data.size() == 0 && brdnumbers.size() != 0");
			rootReponseArray = brdRspUtil.setNoDataFoundResponse(rootReponseArray, brdRequest, brdnumbers, listBrdArray,
					invalidBrdnumbers, numberOfEntries, brdArray);

		} else {
			rootReponseArray = returnValidatedRespondCode(rootReponseArray, brdRequest, brdnumbers, listBrdArray,
					invalidBrdnumbers, numberOfEntries);
		}
		return rootReponseArray;
	}

	@SuppressWarnings({})
	public JSONArray hold_94(BrdRequest brdRequest, String clientName) {
		// TODO: Remove tight coupling and create utility for looping using JDk1.8
		String numberEntries = (null != brdRequest.getNumberOfEntries() && !brdRequest.getNumberOfEntries().isEmpty()
				? brdRequest.getNumberOfEntries()
				: "0");
		Integer numberOfEntries = Integer.parseInt(numberEntries);
		BrdRequestArray[] brdArray = brdRequest.getBrdArray();
		List<String> brdnumbers = new ArrayList<>();
		List<String> invalidBrdnumbers = new ArrayList<>();
		for (int i = 0; i < brdArray.length; i++) {
			String brdnumber = brdArray[i].getBrdNbr().toString().trim();
			if (brdnumber.length() == 15) {
				brdnumbers.add(brdnumber);
			} else {
				invalidBrdnumbers.add(brdnumber);
			}
		}

//		String queryString = brdUtility.getQuery(brdnumbers);
//		List<BrdModel> data = brdUtility.getDataFromPCC(queryString);
		
		List<BrdModel> data = findBrdsByBrdNbrs(brdnumbers);

		JSONArray rootReponseArray = new JSONArray();
		JSONArray listBrdArray = new JSONArray();
		if (data.size() > 0) {
			rootReponseArray = brdRspUtil.addTypOrdrStore(rootReponseArray, brdRequest);
			// rootReponseArray.add(Integer.toString(data.size()));
			for (int i = 0; i < data.size(); i++) {
				log.info(" 94 Data found ");
				JSONObject obj = new JSONObject();
				obj.put("brdNbr", data.get(i).getBrd().getBrdnumber());
				obj = setHoldResponseCode(brdRequest, data, obj, i, "94", clientName);
				listBrdArray.put(obj);
			}
		}
		if (data.size() == 0 && brdnumbers.size() == 0) {
			log.info(" setEmptyResponse  ");
			rootReponseArray = brdRspUtil.setEmptyResponse(brdRequest);
		} else if (brdArray.length > 0 && data.size() == 0 && brdnumbers.size() != 0) {
			log.info(" brdArray.length > 0 && data.size() == 0 && brdnumbers.size() != 0");
			rootReponseArray = brdRspUtil.setNoDataFoundResponse(rootReponseArray, brdRequest, brdnumbers, listBrdArray,
					invalidBrdnumbers, numberOfEntries, brdArray);

		} else {
			rootReponseArray = returnValidatedRespondCode(rootReponseArray, brdRequest, brdnumbers, listBrdArray,
					invalidBrdnumbers, numberOfEntries);
		}
		return rootReponseArray;
	}

	private JSONObject setHoldResponseCode(BrdRequest brdRequest, List<BrdModel> data, JSONObject obj, int i, String QC,
			String clientName) {

		log.info("setHoldResponseCode " + clientName);
		BrdRequestArray[] brdArray = brdRequest.getBrdArray();
		List<String> brdnumbers = new ArrayList<>();
		Date current_date = new Date();
		String modified_current_date = "";
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		try {
			current_date = formatter.parse(formatter.format(current_date));
			modified_current_date = formatter.format(current_date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (brdUtility.isBrdExpired(data.get(i).getBrd().getAttributes().getExpirationDate(), QC)) {
			obj.put("brdAmt", data.get(i).getBrd().getAttributes().getCycleAmtRewarded());
			obj.put("brdResp", "001");
		} else {
			Map<String, BrdSaveResponse> brdSaveResponses = new HashMap<String, BrdSaveResponse>();
			for (int j = 0; j < brdArray.length; j++) {

				String brdnumber = brdArray[j].getBrdNbr().toString().trim();
				brdnumbers.add(brdnumber);
				//BrdSaveResponse brdSaveResponse = new BrdSaveResponse();
				brdSaveResponse.setBrdAmt(brdArray[j].getBrdAmt());
				log.info("Adding key " + brdnumber);
				brdSaveResponses.put(brdnumber, brdSaveResponse);
			}

			log.info("Key " + data.get(i).getBrd().getBrdnumber());
			BrdSaveResponse brdModifiedSaveResponse = brdSaveResponses.get(data.get(i).getBrd().getBrdnumber());
			// log.info( "is NUL " null==brdModifiedSaveResponse);
			String inputbrdAmt = brdModifiedSaveResponse.getBrdAmt();
			log.info("inputbrdAmt " + inputbrdAmt);
			Double inputbrdAmtNumber = (null != inputbrdAmt && !inputbrdAmt.isEmpty()) ? Double.parseDouble(inputbrdAmt)
					: 0.00;
			Double cycleAmtRewardedNumber = (null != data.get(i).getBrd().getAttributes().getCycleAmtRewarded()
					&& !data.get(i).getBrd().getAttributes().getCycleAmtRewarded().isEmpty())
							? Double.parseDouble(data.get(i).getBrd().getAttributes().getCycleAmtRewarded())
							: 0.00;

			log.info("setHoldResponseCode Step 1");

			String brdAmtVariable = "0.00";
			if (inputbrdAmtNumber > cycleAmtRewardedNumber) {
				log.info("setHoldResponseCode Step 2");
				brdAmtVariable = data.get(i).getBrd().getAttributes().getCycleAmtRewarded();
				obj.put("brdAmt", data.get(i).getBrd().getAttributes().getCycleAmtRewarded());
			} else {
				log.info("setHoldResponseCode Step 3  save data");
				obj.put("brdAmt", inputbrdAmt);
				brdAmtVariable = inputbrdAmt;
			}

			Double brdAmtVariableNumber = Double.parseDouble(brdAmtVariable.toString());

			if (data.get(i).getBrd().getAttributes().getReasonCode().equalsIgnoreCase("A")
					|| data.get(i).getBrd().getAttributes().getReasonCode().equalsIgnoreCase("L")) {
				if (brdUtility.isRedemption_DtGtZero(data.get(i).getBrd().getAttributes().getRedemptionDt())
						|| data.get(i).getBrd().getAttributes().getWebHoldFlag().equalsIgnoreCase("Y")) {
					log.info("setHoldResponseCode Step 4 ");
					obj.put("brdAmt", "0.00");
					obj.put("brdResp", "002");
				} else {
					obj.put("brdResp", "000");
					// Amt Reward Used=Amount variable + Amt Reward Used.
					Double amtRewardUsed = (brdAmtVariableNumber
							+ Double.parseDouble(data.get(i).getBrd().getAttributes().getAmtRewardUsed()));
					attributes.setAmtRewardUsed(amtRewardUsed.toString());
					//attributes.setAmtRewardUsed(String.valueOf(Math.round(amtRewardUsed)));
					attributes.setWebHoldDate(modified_current_date);
					attributes.setWebHoldFlag("Y");
					// set last modified date
					attributes.setLastModifiedDate(modified_current_date);
					attributes = brdAttributesUtil.setHoldAttributes(attributes, data, i, clientName, brdRequest);

					boolean success = brdUtility.save(brdData, brd, attributes, data, this.brdRepository, i);

					// Save:End
					// Amount output parameter = cycle amount rewarded.
					obj.put("brdAmt", data.get(i).getBrd().getAttributes().getCycleAmtRewarded());
					log.info("setHoldResponseCode Step 5  savin all data");
				}
			} else {
				obj.put("brdAmt", data.get(i).getBrd().getAttributes().getCycleAmtRewarded());
				obj.put("brdResp",
						brdUtility.setExpiredResponse(data.get(i).getBrd().getAttributes().getExpirationDate(), QC));
			}
		}
		return obj;
	}

	private JSONObject setReversalResponseCode(BrdRequest brdRequest, List<BrdModel> data, JSONObject obj, int i,
			String QC, String clientName) {
		BrdRequestArray[] brdArray = brdRequest.getBrdArray();
		List<String> brdnumbers = new ArrayList<>();
		Date current_date = new Date();
		String modified_current_date = "";
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		try {
			current_date = formatter.parse(formatter.format(current_date));
			modified_current_date = formatter.format(current_date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (brdUtility.isBrdExpired(data.get(i).getBrd().getAttributes().getExpirationDate(), QC)) {
			obj.put("brdAmt", data.get(i).getBrd().getAttributes().getCycleAmtRewarded());
			obj.put("brdResp", "001");
		} else {
			Map<String, BrdSaveResponse> brdSaveResponses = new HashMap<>();
			for (int j = 0; j < brdArray.length; j++) {
				String brdnumber = brdArray[j].getBrdNbr().toString().trim();
				brdnumbers.add(brdnumber);
				//BrdSaveResponse brdSaveResponse = new BrdSaveResponse();
				brdSaveResponse.setBrdAmt(brdArray[j].getBrdAmt());
				brdSaveResponse.setPurchaseAmt(brdArray[j].getBrdAmt());
				brdSaveResponse.setAmtRewardUsed(brdArray[j].getBrdAmt());
				brdSaveResponse.setPurchaseStore(brdRequest.getStoreNbr());
				brdSaveResponses.put(brdnumber, brdSaveResponse);
			}

			BrdSaveResponse brdModifiedSaveResponse = brdSaveResponses.get(data.get(i).getBrd().getBrdnumber());
			String inputbrdAmt = brdModifiedSaveResponse.getBrdAmt();
			Double inputbrdAmtNumber = (null != inputbrdAmt && !inputbrdAmt.isEmpty()) ? Double.parseDouble(inputbrdAmt)
					: 0.00;
			Double cycleAmtRewardedNumber = (null != data.get(i).getBrd().getAttributes().getCycleAmtRewarded()
					&& !data.get(i).getBrd().getAttributes().getCycleAmtRewarded().isEmpty())
							? Double.parseDouble(data.get(i).getBrd().getAttributes().getCycleAmtRewarded())
							: 0.00;

			// IF WEB_HOLD_FLAG='Y' THEN SET WEB_HOLD_FLAG='C' and WEB_HOLD_DATE=0
			if (data.get(i).getBrd().getAttributes().getWebHoldFlag().equalsIgnoreCase("Y")
					|| data.get(i).getBrd().getAttributes().getWebHoldFlag().equalsIgnoreCase("R")) {
				log.info("93 WEB_HOLD_FLAG='Y or 'R' ");
				attributes.setWebHoldFlag("C");
				attributes.setWebHoldDate("0");

			} else {
				attributes.setWebHoldFlag(data.get(i).getBrd().getAttributes().getWebHoldFlag());
				attributes.setWebHoldDate(data.get(i).getBrd().getAttributes().getWebHoldDate());
			}

			String brdAmtVariable = "0.00";

			if (data.get(i).getBrd().getAttributes().getWebHoldFlag().equalsIgnoreCase("Y")
					&& data.get(i).getBrd().getAttributes().getTransNum().equalsIgnoreCase("N")) {
				obj.put("brdAmt", "0.00");
				obj.put("brdResp", "002");
			} else if (inputbrdAmtNumber > cycleAmtRewardedNumber) {
				brdAmtVariable = data.get(i).getBrd().getAttributes().getCycleAmtRewarded();
				obj.put("brdAmt", data.get(i).getBrd().getAttributes().getCycleAmtRewarded());
			} else {
				obj.put("brdAmt", inputbrdAmt);
				brdAmtVariable = inputbrdAmt;
			}

			if (data.get(i).getBrd().getAttributes().getReasonCode().equalsIgnoreCase("A")
					|| data.get(i).getBrd().getAttributes().getReasonCode().equalsIgnoreCase("L")) {

				obj.put("brdAmt", brdAmtVariable);
				attributes = brdAttributesUtil.setReversalAttributes(attributes, data, i, clientName, brdRequest);
				// IF (STORE_OF_PURCHASE =Input Store Parameter and REGISTER_NUMBER=Input
				// terminal Number and TRANS_NUMBER=Input Trans Number)
				if (brdModifiedSaveResponse.getPurchaseStore().equalsIgnoreCase(brdRequest.getStoreNbr())) {
					// save
					obj.put("brdResp", "000");
					// attributes.setWebHoldDate(data.get(i).getBrd().getAttributes().getWebHoldDate());
					attributes.setPurchaseAmt("0.00");
					attributes.setAmtRewardUsed("0.00");
					attributes.setTransNum("0");
					attributes.setRegisterNumber("0");
					attributes.setPurchaseStore("0");
					attributes.setRedemptionDt("0");
					// set last modified date
					attributes.setLastModifiedDate(modified_current_date);

				} else {
					obj.put("brdResp", "000");
					// Amount of purchase=Input Amount parameter
					attributes.setPurchaseAmt(brdModifiedSaveResponse.getPurchaseAmt());
					// Amt Reward Used=Amount variable
					attributes.setAmtRewardUsed(brdAmtVariable);
//                    Store of purchase=Input Store
					attributes.setPurchaseStore(brdModifiedSaveResponse.getPurchaseStore());

					attributes.setRedemptionDt(data.get(i).getBrd().getAttributes().getRedemptionDt());

					// set last modified date
					attributes.setLastModifiedDate(modified_current_date);
					
					attributes.setTransNum("");
					attributes.setRegisterNumber("");
				}

				boolean success = brdUtility.save(brdData, brd, attributes, data, this.brdRepository, i);
			} // IF A and L
		}
		return obj;
	}

	@SuppressWarnings({})
	public JSONArray reedem_92(BrdRequest brdRequest, String clientName) {
		// TODO: Remove tight coupling and create utility for looping using JDk1.8
		String numberEntries = (null != brdRequest.getNumberOfEntries() && !brdRequest.getNumberOfEntries().isEmpty()
				? brdRequest.getNumberOfEntries()
				: "0");
		Integer numberOfEntries = Integer.parseInt(numberEntries);
		BrdRequestArray[] brdArray = brdRequest.getBrdArray();
		List<String> brdnumbers = new ArrayList<>();
		List<String> invalidBrdnumbers = new ArrayList<>();
		for (int i = 0; i < brdArray.length; i++) {
			String brdnumber = brdArray[i].getBrdNbr().toString().trim();
			if (brdnumber.length() == 15) {
				brdnumbers.add(brdnumber);
			} else {
				invalidBrdnumbers.add(brdnumber);
			}
		}

//		String queryString = brdUtility.getQuery(brdnumbers);
//		List<BrdModel> data = brdUtility.getDataFromPCC(queryString);
		
		List<BrdModel> data = findBrdsByBrdNbrs(brdnumbers);

		JSONArray rootReponseArray = new JSONArray();
		JSONArray listBrdArray = new JSONArray();
		if (data.size() > 0) {
			rootReponseArray = brdRspUtil.addTypOrdrStore(rootReponseArray, brdRequest);
			// rootReponseArray.add(Integer.toString(data.size()));
			for (int i = 0; i < data.size(); i++) {
				JSONObject obj = new JSONObject();
				obj.put("brdNbr", data.get(i).getBrd().getBrdnumber());
				obj = setReedemResponseCode(brdRequest, data, obj, i, "92", clientName);
				listBrdArray.put(obj);
			}
		}
		if (data.size() == 0 && brdnumbers.size() == 0) {
			log.info(" setEmptyResponse  ");
			rootReponseArray = brdRspUtil.setEmptyResponse(brdRequest);
		} else if (brdArray.length > 0 && data.size() == 0 && brdnumbers.size() != 0) {
			log.info(" brdArray.length > 0 && data.size() == 0 && brdnumbers.size() != 0");
			rootReponseArray = brdRspUtil.setNoDataFoundResponse(rootReponseArray, brdRequest, brdnumbers, listBrdArray,
					invalidBrdnumbers, numberOfEntries, brdArray);

		} else {
			rootReponseArray = returnValidatedRespondCode(rootReponseArray, brdRequest, brdnumbers, listBrdArray,
					invalidBrdnumbers, numberOfEntries);
		}
		return rootReponseArray;
	}

	private JSONObject setResponseCode(List<BrdModel> data, JSONObject obj, int i, String QC) {
		// Expired BRD is priority for everyone.
		if (brdUtility.isBrdExpired(data.get(i).getBrd().getAttributes().getExpirationDate(), QC)) {
			obj.put("brdAmt", "0.00");
			obj.put("brdResp", "002");
		} else {

			if (data.get(i).getBrd().getAttributes().getReasonCode().equalsIgnoreCase("A")
					|| data.get(i).getBrd().getAttributes().getReasonCode().equalsIgnoreCase("L")) {

				if (brdUtility.isRedemption_DtGtZero(data.get(i).getBrd().getAttributes().getRedemptionDt())
						|| data.get(i).getBrd().getAttributes().getWebHoldFlag().equalsIgnoreCase("Y")) {
					if(data.get(i).getBrd().getAttributes().getRedemptionDt() == null) {
						log.info("Redemption Dt is null");
					}
					log.info("Redemption Dt is "+data.get(i).getBrd().getAttributes().getRedemptionDt());
					obj.put("brdAmt", "0.00");
					obj.put("brdResp", "002");
				} else {
					if (QC.equalsIgnoreCase("95")) {
						if (!brdUtility.isBrdExpired(data.get(i).getBrd().getAttributes().getExpirationDate(), QC)) {
							obj.put("brdAmt", data.get(i).getBrd().getAttributes().getCycleAmtRewarded());
							obj.put("brdResp", "000");
						} else {
							obj.put("brdAmt", data.get(i).getBrd().getAttributes().getCycleAmtRewarded());
							obj.put("brdResp", brdUtility
									.setExpiredResponse(data.get(i).getBrd().getAttributes().getExpirationDate(), QC));
						}
					} else {
						obj.put("brdAmt", data.get(i).getBrd().getAttributes().getCycleAmtRewarded());
						obj.put("brdResp", "000");
					}
				}
			} else {
				obj.put("brdAmt", data.get(i).getBrd().getAttributes().getCycleAmtRewarded());
				obj.put("brdResp",
						brdUtility.setExpiredResponse(data.get(i).getBrd().getAttributes().getExpirationDate(), QC));
			}

		}
		return obj;
	}

	private JSONObject setReedemResponseCode(BrdRequest brdRequest, List<BrdModel> data, JSONObject obj, int i,
			String QC, String clientName) {

		BrdRequestArray[] brdArray = brdRequest.getBrdArray();
		List<String> brdnumbers = new ArrayList<>();
		Date current_date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		String modified_current_date = "";
		try {
			current_date = formatter.parse(formatter.format(current_date));
			modified_current_date = formatter.format(current_date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (brdUtility.isBrdExpired(data.get(i).getBrd().getAttributes().getExpirationDate(), QC)) {
			obj.put("brdAmt", data.get(i).getBrd().getAttributes().getCycleAmtRewarded());
			obj.put("brdResp", "001");
		} else {
			Map<String, BrdSaveResponse> brdSaveResponses = new HashMap<>();
			for (int j = 0; j < brdArray.length; j++) {
				String brdnumber = brdArray[j].getBrdNbr().toString().trim();
				brdnumbers.add(brdnumber);
			//	 BrdSaveResponse brdSaveResponse = new BrdSaveResponse();
				brdSaveResponse.setBrdAmt(brdArray[j].getBrdAmt());
				brdSaveResponse.setPurchaseAmt(brdArray[j].getBrdAmt());
				brdSaveResponse.setPurchaseStore(brdRequest.getStoreNbr());
				// Date of redemption = Current date in Julian .
				brdSaveResponse.setRedemptionDt(modified_current_date);
				// IF WEB_HOLD_FLAG='Y' THEN update WEB_HOLD_FLAG='R'
				brdSaveResponse.setWebHoldFlag(
						data.get(i).getBrd().getAttributes().getWebHoldFlag().equalsIgnoreCase("Y") ? "R"
								: data.get(i).getBrd().getAttributes().getWebHoldFlag());
				brdSaveResponses.put(brdnumber, brdSaveResponse);
			}

			String brdAmtVariable = "0.00";
			BrdSaveResponse brdModifiedSaveResponse = brdSaveResponses.get(data.get(i).getBrd().getBrdnumber());
			if(brdModifiedSaveResponse== null) {
				brdModifiedSaveResponse = new BrdSaveResponse();
			}
			String inputbrdAmt = brdModifiedSaveResponse.getBrdAmt();
			Double inputbrdAmtNumber = (null != inputbrdAmt && !inputbrdAmt.isEmpty()) ? Double.parseDouble(inputbrdAmt)
					: 0.00;
			Double cycleAmtRewardedNumber = (null != data.get(i).getBrd().getAttributes().getCycleAmtRewarded()
					&& !data.get(i).getBrd().getAttributes().getCycleAmtRewarded().isEmpty())
							? Double.parseDouble(data.get(i).getBrd().getAttributes().getCycleAmtRewarded())
							: 0.00;

			if (inputbrdAmtNumber > cycleAmtRewardedNumber) {
				obj.put("brdAmt", data.get(i).getBrd().getAttributes().getCycleAmtRewarded());
				brdAmtVariable = data.get(i).getBrd().getAttributes().getCycleAmtRewarded();
			} else {
				// Update CycleAmtRewarded with input inputbrdAmt
				obj.put("brdAmt", inputbrdAmt);
				brdAmtVariable = inputbrdAmt;
			}

			if (data.get(i).getBrd().getAttributes().getReasonCode().equalsIgnoreCase("A")
					|| data.get(i).getBrd().getAttributes().getReasonCode().equalsIgnoreCase("L")) {
				if (brdUtility.isRedemption_DtGtZero(data.get(i).getBrd().getAttributes().getRedemptionDt())) {
					obj.put("brdAmt", "0.00");
					obj.put("brdResp", "002");
				} else {
					// Return status=000
					obj.put("brdResp", "000");
					// Save:Start
					// Amount of purchase=Input Amount parameter
					attributes.setPurchaseAmt(brdModifiedSaveResponse.getPurchaseAmt());
					// Amt Reward Used=Amount variable
					attributes.setAmtRewardUsed(brdAmtVariable);
					// Store of purchase=Store
					attributes.setPurchaseStore(brdModifiedSaveResponse.getPurchaseStore());
					// Date of redemption = Current date in Julian .
					attributes.setRedemptionDt(brdModifiedSaveResponse.getRedemptionDt());
					// IF WEB_HOLD_FLAG='Y' THEN update WEB_HOLD_FLAG='R'
					attributes.setWebHoldFlag(brdModifiedSaveResponse.getWebHoldFlag());
					// set last modified date
					attributes.setLastModifiedDate(modified_current_date);

					attributes = brdAttributesUtil.setRedeemAttributes(attributes, data, i, clientName, brdRequest);

					boolean success = brdUtility.save(brdData, brd, attributes, data, this.brdRepository, i);
					// Save:End
					// Amount output parameter = cycle amount rewarded.
					obj.put("brdAmt", data.get(i).getBrd().getAttributes().getCycleAmtRewarded());
				}
			} else {
				obj.put("brdAmt", data.get(i).getBrd().getAttributes().getCycleAmtRewarded());
				obj.put("brdResp",
						brdUtility.setExpiredResponse(data.get(i).getBrd().getAttributes().getExpirationDate(), QC));
			}

		}
		return obj;
	}

	public JSONArray verifyIfAvailable_95(BrdRequest brdRequest) {

		String token = brdRequest.getToken();

	//  List<BrdModel> data = this.brdRepository.findBrdByToken(token);
		List<BrdModel> data = findBrdsByToken(token);
		
		JSONArray rootReponseArray = new JSONArray();
		JSONArray listBrdArray = new JSONArray();
		if (data.size() > 0) {
			// Read each BRD number for that account number and maximum number of BRD is 30
			int maxNumBRDS = data.size();
			rootReponseArray.put(brdRequest.getType());
			rootReponseArray.put("");
			rootReponseArray.put("");
			rootReponseArray.put(brdRequest.getToken());
			
			//if the total number of BRDs are more than 30, they need to be sorted first based on the redemption and webhold flag and display only 30
			log.info("no. of Brds: "+maxNumBRDS);
			if(maxNumBRDS > 30)	
				data = sortBrdsByValidity(data);
			
			maxNumBRDS = data.size() > 30 ? 30 : data.size();
			rootReponseArray.put(String.valueOf(maxNumBRDS));
				
			for (int i = 0; i < maxNumBRDS; i++) {
				JSONObject obj = new JSONObject();
				
				obj.put("brdNbr", data.get(i).getBrd().getBrdnumber());
				obj = setResponseCode(data, obj, i, "95");
				//Set expiry date only if the BRD is valid
				if(!StringUtils.isEmpty(obj.get("brdResp")) && obj.get("brdResp").equals("000"))
					obj.put("brdExpDate",
						brdUtility.getFinalDate(data.get(i).getBrd().getAttributes().getExpirationDate()));
				listBrdArray.put(obj);
			}
			rootReponseArray.put(listBrdArray);
			// return rootReponseArray;
		} else {
			// WHen no matching data found
			rootReponseArray.put(!brdRequest.getType().isEmpty() ? brdRequest.getType() : "");
			rootReponseArray.put("");
			rootReponseArray.put("");
			rootReponseArray.put(!brdRequest.getToken().isEmpty() ? brdRequest.getToken() : "");
			rootReponseArray.put("0");
			rootReponseArray.put(listBrdArray);
		}
		return rootReponseArray;
	}
	
	private List<BrdModel> sortBrdsByValidity(List<BrdModel> brds){
		
		//Excluding the Brds with incomplete data -- null values
		List<BrdModel> filteredBrds = brds.stream()
		.filter(brd -> !Objects.isNull(brd.getBrd().getAttributes().getRedemptionDt()))
		.filter(brd -> !Objects.isNull(brd.getBrd().getAttributes().getExpirationDate()))
		.filter(brd -> !Objects.isNull(brd.getBrd().getAttributes().getReasonCode()))
		.filter(brd -> !Objects.isNull(brd.getBrd().getAttributes().getWebHoldFlag()))
		.collect(Collectors.toList());
		
		//Excluding the BRDs that are not valid for redemtpion
		List<BrdModel> brdsToPrioritise = 
		filteredBrds.stream()
		.filter(brd -> (brd.getBrd().getAttributes().getReasonCode().equals("A") || brd.getBrd().getAttributes().getReasonCode().equals("L")))
		.filter(brd -> (brd.getBrd().getAttributes().getRedemptionDt().equals("0") && !brd.getBrd().getAttributes().getWebHoldFlag().equalsIgnoreCase(("Y"))))
		.filter(brd -> !brdUtility.isBrdExpired(brd.getBrd().getAttributes().getExpirationDate(),"95"))
		.collect(Collectors.toList());
		
		//Sorting rest of the BRDs based on the status, redemption date and webhold flag.
		log.info("No. of Brds to sort: "+brdsToPrioritise.size());
		Comparator<BrdModel> comparator = (brd_1,brd_2) -> brd_1.getBrd().getAttributes().getRedemptionDt().compareTo(brd_2.getBrd().getAttributes().getRedemptionDt());
		comparator = comparator.thenComparing((brd_1,brd_2) -> "Y".compareTo(brd_1.getBrd().getAttributes().getWebHoldFlag()));
		comparator = comparator.thenComparing((brd_1,brd_2) -> brd_1.getBrd().getAttributes().getExpirationDate().compareTo(brd_2.getBrd().getAttributes().getExpirationDate()));
		brdsToPrioritise.sort(comparator);
		
		//appending rest of the BRDs to the sorted ones.
		brds.removeAll(brdsToPrioritise);
		brdsToPrioritise.addAll(brds);
		brdsToPrioritise.stream().forEach(System.out::println);
		return brdsToPrioritise;
	}

	// BUild JSON response if some one wants route to this location
	public BrdResponse response(BrdModel brdData, BrdRequest brdRequest) {
		brdResponse.setBrdNbr(brdData.getBrd().getBrdnumber());
		// brdResponse.setTokennumber(tokennumber);
		brdResponse.setBrdAmt(brdData.getBrd().getAttributes().getCycleAmtRewarded());
		brdResponse.setBrdResp("000");

		brdResponse.setRsType(brdRequest.getType());
		brdResponse.setRsOrderNumber(brdRequest.getOrderNumber());
		brdResponse.setRsStoreNbr(brdRequest.getStoreNbr());
		brdResponse.setRsNumberOfEntries(brdRequest.getNumberOfEntries());
		return brdResponse;
	}
	
	public List<BrdModel> findBrdsByToken(String id) {
		/*
		return findAll().stream()
				.filter(brdModel -> brdModel.getBrd().getTokennumber().equalsIgnoreCase(id))
				.collect(Collectors.toList());
		*/
		BrdModel model = null;
		Brd brd = null;
		Attributes attributes = null;
		List<BrdModel> models = new ArrayList<BrdModel>();
		//Using Jedis & Set
		/*
		Jedis jedis = new Jedis("localhost", 6379);
		Set<String> smembers = jedis.smembers("tokennumber:"+id);
		*/
		
		// Using Lettuce & Set
		/* Set<String> smembers = syncCommands.smembers("tokennumber:"+id);*/
		
		// Using Lettuce & Sorted Set
		List<String> smembers = syncCommands.zrange("tokennumber:"+id, 0, -1);
		for(String hash : smembers) {
			model = new BrdModel();
			brd = new Brd();
			attributes = new Attributes();
//			Map<String, String> all = jedis.hgetall(hash);
			Map<String, String> all = syncCommands.hgetall(hash);
			for (Map.Entry<String, String> entry : all.entrySet()) {
				brdUtility.hashMapToBrd(brd, attributes, entry);
			}
			brd.setAttributes(attributes);
			model.setBrd(brd);
			models.add(model);
		}
		return models;
	}

	public List<BrdModel> findBrdsByBrdNbrs(List<String> brdnumbers) {
		/*
		return findAll().stream()
				.filter(brdModel -> String.join(",", brdNumbers).contains(brdModel.getBrd().getBrdnumber()))
				.collect(Collectors.toList());
		*/
		BrdModel model = null;
		Brd brd = null;
		Attributes attributes = null;
		List<BrdModel> models = new ArrayList<BrdModel>();
		
//		Jedis jedis = new Jedis("localhost", 6379);
		for (String brdnum : brdnumbers) {
//			Map<String, String> hgetAll = jedis.hgetAll("brdnumber:" + brdnum);
			Map<String, String> hgetAll = this.syncCommands.hgetall("brdnumber:" + brdnum);
			model = new BrdModel();
			brd = new Brd();
			attributes = new Attributes();

			for (Map.Entry<String, String> entry : hgetAll.entrySet()) {
				brdUtility.hashMapToBrd(brd, attributes, entry);
			}
			brd.setAttributes(attributes);
			model.setBrd(brd);
			models.add(model);			
		}
		return models;
	}

	/*
	private List<BrdModel> findAll() {
		List<BrdModel> result = new ArrayList<>();
		this.brdRepository.findAll().forEach(result::add);
		return result;
	}
	*/
	
	
}
