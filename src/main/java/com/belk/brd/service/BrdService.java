package com.belk.brd.service;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.belk.brd.model.BrdRequest;
import com.belk.brd.model.BrdResponse;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BrdService {

	@Autowired
	BrdResponse brdResponse;

	@Autowired
	BrdServiceHandler brdServiceHandler;
	private JSONArray responseJsonArray;
	
	// JSON Handling
	public JSONArray processBRD(BrdRequest brdRequest, String clientName) {
		log.info(" brdRequest TYPE == " + brdRequest.getType().toString().trim());
		if (brdRequest.getType().toString().trim().equalsIgnoreCase("90")) {
			responseJsonArray = inquiry(brdRequest);
		}
		if (brdRequest.getType().toString().trim().equalsIgnoreCase("92")) {
			responseJsonArray = reedemBRD(brdRequest, clientName);
		} else if (brdRequest.getType().toString().trim().equalsIgnoreCase("93")) {
			responseJsonArray = reversalBRD(brdRequest, clientName);
		}
		if (brdRequest.getType().toString().trim().equalsIgnoreCase("94")) {
			responseJsonArray = holdBRD(brdRequest, clientName);
		} else if (brdRequest.getType().toString().trim().equalsIgnoreCase("95")) {
			responseJsonArray = verifyIfAvailable(brdRequest);
		}

		return responseJsonArray;
	}

	private JSONArray holdBRD(BrdRequest brdRequest, String clientName) {
		log.info("holdBRD" + "94 ");
		responseJsonArray = brdServiceHandler.hold_94(brdRequest, clientName);
		return responseJsonArray;
	}

	private JSONArray reversalBRD(BrdRequest brdRequest, String clientName) {
		log.info("reversalBRD" + "93 ");
		responseJsonArray = brdServiceHandler.reversal_93(brdRequest, clientName);
		return responseJsonArray;
	}

	private JSONArray reedemBRD(BrdRequest brdRequest, String clientName) {
		log.info("reedemBRD" + "92 ");
		responseJsonArray = brdServiceHandler.reedem_92(brdRequest, clientName);
		return responseJsonArray;
	}

	public JSONArray inquiry(BrdRequest brdRequest) {
		log.info("validateBRDNumber" + "90 ");
		responseJsonArray = brdServiceHandler.inquiry_90(brdRequest);
		return responseJsonArray;

	}

	public JSONArray verifyIfAvailable(BrdRequest brdRequest) {
		log.info("verifyIfAvailable" + "95 ");
		responseJsonArray = brdServiceHandler.verifyIfAvailable_95(brdRequest);
		return responseJsonArray;
	}
	
}
