package com.belk.brd.util;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.belk.brd.model.BrdRequest;
import com.belk.brd.model.BrdRequestArray;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class BrdRspUtil {

	// if no data found for Key response

	public JSONArray setEmptyResponse(BrdRequest brdRequest) {

//		log.info(" Add Invalid returnValidatedRespondCode");
//		// Last, add Invalid BRD"s to list as it is.
//		if (invalidBrdnumbers.size() > 0) {
//			for (int invalid = 0; invalid < invalidBrdnumbers.size(); invalid++) {
//				log.info(" invalidBrdnumbers.size() " + invalidBrdnumbers.size());
//				JSONObject inavlidObj = new JSONObject();
//				inavlidObj.put("brdNbr", invalidBrdnumbers.get(invalid));
//				inavlidObj.put("brdAmt", "0.00");
//				inavlidObj.put("brdResp", "001");
//				listBrdArray.add(inavlidObj);
//			}
//			return rootReponseArray;
//		}else
		{
			JSONArray emptyReponseArray = new JSONArray();
			JSONArray emptylistBrdArray = new JSONArray();
			emptyReponseArray.put(!brdRequest.getType().isEmpty() ? brdRequest.getType() : "");
			emptyReponseArray.put("");
			emptyReponseArray.put("");
			if (brdRequest.getType().toString().trim().equalsIgnoreCase("95")) {
				emptyReponseArray.put(!brdRequest.getToken().isEmpty() ? brdRequest.getToken() : "");
			} else {
				emptyReponseArray
						.put((brdRequest.getBrdArray()).length > 0 && null != brdRequest.getBrdArray()[0].getBrdNbr()
								&& !brdRequest.getBrdArray()[0].getBrdNbr().isEmpty()
										? brdRequest.getBrdArray()[0].getBrdNbr()
										: "");
			}
			emptyReponseArray.put("0");
			emptyReponseArray.put(emptylistBrdArray);

			return emptyReponseArray;
		}
	}

	public JSONArray setEmptyRspForInvalidRq(BrdRequest brdRequest) {
		JSONArray rootReponseArray = new JSONArray();
		JSONArray listBrdArray = new JSONArray();
		if (null != brdRequest) {
			rootReponseArray.put(!brdRequest.getType().isEmpty() ? brdRequest.getType() : "");
			rootReponseArray.put("");
			rootReponseArray.put("");
			if (brdRequest.getType().toString().trim().equalsIgnoreCase("95")) {
				rootReponseArray.put(!brdRequest.getToken().isEmpty() ? brdRequest.getToken() : "");
			} else {
				rootReponseArray
						.put((brdRequest.getBrdArray()).length > 0 && null != brdRequest.getBrdArray()[0].getBrdNbr()
								&& !brdRequest.getBrdArray()[0].getBrdNbr().isEmpty()
										? brdRequest.getBrdArray()[0].getBrdNbr()
										: "");
			}
			rootReponseArray.put("0");
			rootReponseArray.put(listBrdArray);
		} else {
			rootReponseArray = setEmptyRspForFullInvalidRq();

		}
		return rootReponseArray;
	}

	public JSONArray setEmptyRspForFullInvalidRq() {

		JSONArray rootReponseArray = new JSONArray();
		JSONArray listBrdArray = new JSONArray();
		rootReponseArray.put("");
		rootReponseArray.put("");
		rootReponseArray.put("");
		rootReponseArray.put("0");
		rootReponseArray.put(listBrdArray);
		return rootReponseArray;
	}

	public Boolean isRqValid(BrdRequest brdRequest) {
		boolean validRq = true;
		log.info("Val " + " Start");
		if (null != brdRequest) {
			// Check if Type is correct
			if (!brdRequest.getType().isEmpty()) {
				if (brdRequest.getType().toString().trim().equalsIgnoreCase("90")
						|| brdRequest.getType().toString().trim().equalsIgnoreCase("92")
						|| brdRequest.getType().toString().trim().equalsIgnoreCase("93")
						|| brdRequest.getType().toString().trim().equalsIgnoreCase("94")
						|| brdRequest.getType().toString().trim().equalsIgnoreCase("95")) {

					// if correct operation check combination for BRD and Token don't accept token
					// or
					// any other value other than BRD
					if (!brdRequest.getType().toString().trim().equalsIgnoreCase("95")) {
						BrdRequestArray[] brdArray = brdRequest.getBrdArray();
						List<String> brdnumbers = new ArrayList<>();
						// At least one BRD array need to present
						if (null == brdArray) {
							log.info("Val " + "null==brdArray");
							return false;
						} else if (brdArray.length == 0) {
							log.info("Val " + "brdArray.length == 0");
							return false;
						} else {
							for (int i = 0; i < brdArray.length; i++) {
								if (null == brdArray[i].getBrdNbr()) {
									log.info("null==brdArray[i].getBrdNbr()");
									return false;
								}
								String brdnumber = brdArray[i].getBrdNbr().toString().trim();
								brdnumbers.add(brdnumber);
							}
						}
						// at least one BRD need to be present for all except 95
						if (null != brdnumbers && brdnumbers.size() == 0) {
							log.info("Val " + "null!=brdnumbers && brdnumbers.size() == 0");
							return false;
						}
					} else {
						if (null == brdRequest.getToken() || brdRequest.getToken().isEmpty()) {
							log.info(
									"Val " + "null == brdRequest.getToken() || brdRequest.getToken().isEmpty()");
							return false;
						}
						// For testing removed this , for pROD enable this
						else {
							// if token present, Check token length, if not 16 then resturn emty result
							if (brdRequest.getToken().toString().length() != 16) {
								return false;
							}
						}
					}

				} else {
					// none of the valid operation present send empty response
					log.info("Val " + "Invalid Operation Type");
					return false;
				}
			} else {
				log.info("Val " + "brdRequest.getType().isEmpty()");
				return false;
			}
		} else {
			log.info("Val " + "brdRequest is NULL ");
			return false;
		}

		// Do not Check BRD length is 15 or not, gives empty response, try this exmaple
		// one valid and one with 14 digit

		// Check token is present for 95, if not send empty response
		if (brdRequest.getType().toString().trim().equalsIgnoreCase("95")) {
			if (null == brdRequest.getToken() || brdRequest.getToken().isEmpty()) {
				log.info("Val " + "null == brdRequest.getToken() || brdRequest.getToken().isEmpty()");
				return false;
			}
			// IN PROD Enable
			else {
				// if token present, Check token length, if not 16 then resturn emty result
				if (brdRequest.getToken().toString().length() != 16) {
					log.info("Val " + "brdRequest.getToken().toString().length() != 16");
					return false;
				}
			}
		}
		return validRq;
	}

	public JSONArray addTypOrdrStore(JSONArray rootReponseArray, BrdRequest brdRequest) {
		rootReponseArray.put(brdRequest.getType());
		rootReponseArray.put(null != brdRequest.getOrderNumber() && !brdRequest.getOrderNumber().isEmpty()
				? brdRequest.getOrderNumber()
				: "");
		rootReponseArray
				.put(null != brdRequest.getStoreNbr() && !brdRequest.getStoreNbr().isEmpty() ? brdRequest.getStoreNbr()
						: "");
		rootReponseArray.put("");// PLace for Token NUmber
		rootReponseArray.put(null != brdRequest.getNumberOfEntries() && !brdRequest.getNumberOfEntries().isEmpty()
				? brdRequest.getNumberOfEntries()
				: "");
		return rootReponseArray;
	}

	public JSONObject createEmptyBrdObj(JSONObject extraObj) {
		extraObj.put("brdNbr",
				"\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000");
		extraObj.put("brdAmt", "0.00");
		extraObj.put("brdResp", "001");
		return extraObj;
	}

	public JSONArray setNoDataFoundResponse(JSONArray rootReponseArray, BrdRequest brdRequest, List<String> brdnumbers,
			JSONArray listBrdArray, List<String> invalidBrdnumbers, Integer numberOfEntries,
			BrdRequestArray[] brdArray) {

		// BRDS are present, BRD with 15 digit but no reposne from PCC for all of them
		// set upto number of entries. Ste balnk if num entries are greater

		rootReponseArray.put(brdRequest.getType());
		rootReponseArray.put(null != brdRequest.getOrderNumber() && !brdRequest.getOrderNumber().isEmpty()
				? brdRequest.getOrderNumber()
				: "");
		rootReponseArray
				.put(null != brdRequest.getStoreNbr() && !brdRequest.getStoreNbr().isEmpty() ? brdRequest.getStoreNbr()
						: "");
		rootReponseArray.put("");// place of Token number
		rootReponseArray.put(null != brdRequest.getNumberOfEntries() && !brdRequest.getNumberOfEntries().isEmpty()
				? brdRequest.getNumberOfEntries()
				: "");
		//
		if (numberOfEntries <= brdArray.length) {
			for (int h = 0; h < numberOfEntries; h++) {
				JSONObject notFoundObj = new JSONObject();
				notFoundObj.put("brdNbr", brdnumbers.get(h));
				notFoundObj.put("brdAmt", "0.00");
				notFoundObj.put("brdResp", "001");
				listBrdArray.put(notFoundObj);
			}
		} else if (numberOfEntries > brdArray.length) {
			if (listBrdArray.length() == 0) {
				for (int t = 0; t < brdArray.length; t++) {
					JSONObject notFoundObj = new JSONObject();
					notFoundObj.put("brdNbr", brdArray[t].getBrdNbr().toString().trim());
					notFoundObj.put("brdAmt", "0.00");
					notFoundObj.put("brdResp", "001");
					listBrdArray.put(notFoundObj);
				}
			}
			// Add blank elements to listBrdArray
			for (int cnt = brdArray.length; cnt < numberOfEntries; cnt++) {
				log.info(" data.size() Extar count " + cnt);
				JSONObject extraObj = new JSONObject();
				extraObj = createEmptyBrdObj(extraObj);
				listBrdArray.put(extraObj);
			}
		}
		rootReponseArray.put(listBrdArray);

		return rootReponseArray;
	}

}
