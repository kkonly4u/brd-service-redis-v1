package com.belk.brd.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.belk.brd.model.Attributes;
import com.belk.brd.model.BrdModel;
import com.belk.brd.model.BrdRequest;

/**
 * @author jadhaab
 *
 */
@Component
public class BrdAttributesUtil {

	@Autowired
	BrdUtility brdUtility;
	
	public Attributes setHoldAttributes(Attributes attributes, List<BrdModel> data, int i, String clientName, BrdRequest brdRequest) {
		// set rest all remaining attributes default
		attributes.setTransNum(brdUtility.getTransNum(brdRequest.getOrderNumber()));
		attributes.setRegisterNumber(brdUtility.getRegisterNumber(brdRequest.getOrderNumber()));
		attributes.setCycleAmtRewarded(data.get(i).getBrd().getAttributes().getCycleAmtRewarded());
		attributes.setPurchaseAmt(data.get(i).getBrd().getAttributes().getPurchaseAmt());
		attributes.setPurchaseStore(data.get(i).getBrd().getAttributes().getPurchaseStore());
		attributes.setRedemptionDt(data.get(i).getBrd().getAttributes().getRedemptionDt());
		attributes.setIssueDate(data.get(i).getBrd().getAttributes().getIssueDate());
		attributes.setExpirationDate(data.get(i).getBrd().getAttributes().getExpirationDate());
		attributes.setReasonCode(data.get(i).getBrd().getAttributes().getReasonCode());
		attributes.setRedemptionAccountNumber(data.get(i).getBrd().getAttributes().getRedemptionAccountNumber());
		attributes.setLastMaintainedUserid(clientName);
		attributes.setBatchFlag(data.get(i).getBrd().getAttributes().getBatchFlag());
		attributes.setActionFlag(data.get(i).getBrd().getAttributes().getActionFlag());
		attributes.setCreatedDate(data.get(i).getBrd().getAttributes().getCreatedDate());
		attributes.setTypeCd("94");
		return attributes;
	}
	
	public Attributes setReversalAttributes(Attributes attributes, List<BrdModel> data, int i, String clientName,BrdRequest brdRequest) {
		//attributes.setWebHoldDate(data.get(i).getBrd().getAttributes().getWebHoldDate());
		attributes.setTransNum(brdUtility.getTransNum(brdRequest.getOrderNumber()));
		attributes.setRegisterNumber(brdUtility.getRegisterNumber(brdRequest.getOrderNumber()));
		attributes.setIssueDate(data.get(i).getBrd().getAttributes().getIssueDate());
		attributes.setExpirationDate(data.get(i).getBrd().getAttributes().getExpirationDate());
		attributes.setCycleAmtRewarded(data.get(i).getBrd().getAttributes().getCycleAmtRewarded());
		attributes.setReasonCode(data.get(i).getBrd().getAttributes().getReasonCode());
		attributes.setRedemptionAccountNumber(data.get(i).getBrd().getAttributes().getRedemptionAccountNumber());
		attributes.setLastMaintainedUserid(clientName);
		attributes.setBatchFlag(data.get(i).getBrd().getAttributes().getBatchFlag());
		attributes.setActionFlag(data.get(i).getBrd().getAttributes().getActionFlag());
		attributes.setCreatedDate(data.get(i).getBrd().getAttributes().getCreatedDate());
		attributes.setTypeCd("93");
		return attributes;
	}
	
	public Attributes setRedeemAttributes(Attributes attributes, List<BrdModel> data, int i, String clientName, BrdRequest brdRequest) {
		// set rest all remaining attributes default
		attributes.setTransNum(brdUtility.getTransNum(brdRequest.getOrderNumber()));
		attributes.setRegisterNumber(brdUtility.getRegisterNumber(brdRequest.getOrderNumber()));
		attributes.setWebHoldDate(data.get(i).getBrd().getAttributes().getWebHoldDate());
		attributes.setIssueDate(data.get(i).getBrd().getAttributes().getIssueDate());
		attributes.setCycleAmtRewarded(data.get(i).getBrd().getAttributes().getCycleAmtRewarded());
		attributes.setExpirationDate(data.get(i).getBrd().getAttributes().getExpirationDate());
		attributes.setReasonCode(data.get(i).getBrd().getAttributes().getReasonCode());
		attributes.setRedemptionAccountNumber(data.get(i).getBrd().getAttributes().getRedemptionAccountNumber());
		attributes.setLastMaintainedUserid(clientName);
		attributes.setBatchFlag(data.get(i).getBrd().getAttributes().getBatchFlag());
		attributes.setActionFlag(data.get(i).getBrd().getAttributes().getActionFlag());
		attributes.setCreatedDate(data.get(i).getBrd().getAttributes().getCreatedDate());
		attributes.setTypeCd("92");
		return attributes;
	}
}
