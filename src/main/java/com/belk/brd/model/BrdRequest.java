package com.belk.brd.model;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Component
@Getter @Setter @AllArgsConstructor @NoArgsConstructor @ToString
public class BrdRequest {
	
	private String numberOfEntries;

	@NotNull
	@JsonProperty("Type")
	private String type;

	@NotNull
	@JsonProperty(required = true)
	private BrdRequestArray[] brdArray;

	private String orderNumber;

	private String storeNbr;

	private String token;
}
