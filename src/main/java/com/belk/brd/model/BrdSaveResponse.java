package com.belk.brd.model;

import org.springframework.stereotype.Component;

/**
 * @author jadhaab
 *
 */
@Component
public class BrdSaveResponse {

	public BrdSaveResponse() {

	}

	@Override
	public String toString() {
		return "BrdSaveResponse [purchaseAmt=" + purchaseAmt + ", brdAmt=" + brdAmt + ", amtRewardUsed=" + amtRewardUsed
				+ ", transNum=" + transNum + ", registerNumber=" + registerNumber + ", purchaseStore=" + purchaseStore
				+ ", redemptionDt=" + redemptionDt + ", webHoldFlag=" + webHoldFlag + ", webHoldDate=" + webHoldDate
				+ ", issue_date=" + issue_date + ", expirationDate=" + expirationDate + ", cycleAmtRewarded="
				+ cycleAmtRewarded + ", reasonCode=" + reasonCode + ", redemptionAccountNumber="
				+ redemptionAccountNumber + ", lastMaintainedUserid=" + lastMaintainedUserid + ", batchFlag="
				+ batchFlag + ", actionFlag=" + actionFlag + "]";
	}

	public BrdSaveResponse(String purchaseAmt, String brdAmt, String amtRewardUsed, String transNum,
			String registerNumber, String purchaseStore, String redemptionDt, String webHoldFlag, String webHoldDate,
			String issue_date, String expirationDate, String cycleAmtRewarded, String reasonCode,
			String redemptionAccountNumber, String lastMaintainedUserid, String batchFlag, String actionFlag) {

		this.purchaseAmt = purchaseAmt;
		this.brdAmt = brdAmt;
		this.amtRewardUsed = amtRewardUsed;
		this.transNum = transNum;
		this.registerNumber = registerNumber;
		this.purchaseStore = purchaseStore;
		this.redemptionDt = redemptionDt;
		this.webHoldFlag = webHoldFlag;
		this.webHoldDate = webHoldDate;
		this.issue_date = issue_date;
		this.expirationDate = expirationDate;
		this.cycleAmtRewarded = cycleAmtRewarded;
		this.reasonCode = reasonCode;
		this.redemptionAccountNumber = redemptionAccountNumber;
		this.lastMaintainedUserid = lastMaintainedUserid;
		this.batchFlag = batchFlag;
		this.actionFlag = actionFlag;
	}

	String purchaseAmt;
	String brdAmt;
	String amtRewardUsed;
	String transNum;
	String registerNumber;
	String purchaseStore;
	String redemptionDt;
	String webHoldFlag;
	String webHoldDate;

	String issue_date;
	String expirationDate;
	String cycleAmtRewarded;
	String reasonCode;
	String redemptionAccountNumber;
	String lastMaintainedUserid;
	String batchFlag;
	String actionFlag;

	public String getPurchaseAmt() {
		return purchaseAmt;
	}

	public void setPurchaseAmt(String purchaseAmt) {
		this.purchaseAmt = purchaseAmt;
	}

	public String getBrdAmt() {
		return brdAmt;
	}

	public void setBrdAmt(String brdAmt) {
		this.brdAmt = brdAmt;
	}

	public String getAmtRewardUsed() {
		return amtRewardUsed;
	}

	public void setAmtRewardUsed(String amtRewardUsed) {
		this.amtRewardUsed = amtRewardUsed;
	}

	public String getTransNum() {
		return transNum;
	}

	public void setTransNum(String transNum) {
		this.transNum = transNum;
	}

	public String getRegisterNumber() {
		return registerNumber;
	}

	public void setRegisterNumber(String registerNumber) {
		this.registerNumber = registerNumber;
	}

	public String getPurchaseStore() {
		return purchaseStore;
	}

	public void setPurchaseStore(String purchaseStore) {
		this.purchaseStore = purchaseStore;
	}

	public String getRedemptionDt() {
		return redemptionDt;
	}

	public void setRedemptionDt(String redemptionDt) {
		this.redemptionDt = redemptionDt;
	}

	public String getWebHoldFlag() {
		return webHoldFlag;
	}

	public void setWebHoldFlag(String webHoldFlag) {
		this.webHoldFlag = webHoldFlag;
	}

	public String getWebHoldDate() {
		return webHoldDate;
	}

	public void setWebHoldDate(String webHoldDate) {
		this.webHoldDate = webHoldDate;
	}

	public String getIssue_date() {
		return issue_date;
	}

	public void setIssue_date(String issue_date) {
		this.issue_date = issue_date;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getCycleAmtRewarded() {
		return cycleAmtRewarded;
	}

	public void setCycleAmtRewarded(String cycleAmtRewarded) {
		this.cycleAmtRewarded = cycleAmtRewarded;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String getRedemptionAccountNumber() {
		return redemptionAccountNumber;
	}

	public void setRedemptionAccountNumber(String redemptionAccountNumber) {
		this.redemptionAccountNumber = redemptionAccountNumber;
	}

	public String getLastMaintainedUserid() {
		return lastMaintainedUserid;
	}

	public void setLastMaintainedUserid(String lastMaintainedUserid) {
		this.lastMaintainedUserid = lastMaintainedUserid;
	}

	public String getBatchFlag() {
		return batchFlag;
	}

	public void setBatchFlag(String batchFlag) {
		this.batchFlag = batchFlag;
	}

	public String getActionFlag() {
		return actionFlag;
	}

	public void setActionFlag(String actionFlag) {
		this.actionFlag = actionFlag;
	}

}
