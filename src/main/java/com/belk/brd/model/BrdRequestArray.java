package com.belk.brd.model;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Component
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter @Setter
public class BrdRequestArray
{
	@NotNull
	private String brdNbr;
    
    private String brdAmt;
    
}