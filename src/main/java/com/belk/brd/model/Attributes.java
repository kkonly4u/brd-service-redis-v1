package com.belk.brd.model;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "issue_date", "expiration_date", "redemption_Dt", "cycle_amt_rewarded", "amt_reward_used",
		"purchase_amt", "purchase_store", "register_number", "trans_num", "reason_code", "redemption_account_number",
		"last_maintained_userid", "web_hold_flag", "web_hold_date", "batch_flag", "action_flag" })
@Component
@ToString @Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class Attributes implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("issue_date")
	private String issueDate;

	@JsonProperty("expiration_date")
	private String expirationDate;

	@JsonProperty("redemption_Dt")
	private String redemptionDt;

	@JsonProperty("cycle_amt_rewarded")
	private String cycleAmtRewarded;

	@JsonProperty("amt_reward_used")
	private String amtRewardUsed;

	@JsonProperty("purchase_amt")
	private String purchaseAmt;

	@JsonProperty("purchase_store")
	private String purchaseStore;

	@JsonProperty("register_number")
	private String registerNumber;

	@JsonProperty("trans_num")
	private String transNum;

	@JsonProperty("reason_code")
	private String reasonCode;

	@JsonProperty("redemption_account_number")
	private String redemptionAccountNumber;

	@JsonProperty("last_maintained_userid")
	private String lastMaintainedUserid;

	@JsonProperty("web_hold_flag")
	private String webHoldFlag;

	@JsonProperty("web_hold_date")
	private String webHoldDate;

	@JsonProperty("batch_flag")
	private String batchFlag;

	@JsonProperty("action_flag")
	private String actionFlag;

	@JsonProperty("created_date")
	private String createdDate;

	@JsonProperty("last_modified_date")
	private String lastModifiedDate;

	@JsonProperty("type_cd")
	private String typeCd;

}