package com.belk.brd.model;

import org.json.JSONArray;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "brdNbr", "brdAmt", "brdResp", "brdExpDate" })
@JsonIgnoreProperties(ignoreUnknown = true)
@Component
@Getter @Setter @ToString
public class BrdResponse {

	@JsonProperty("brdNbr")
	private String brdNbr;
	
	@JsonProperty("brdAmt")
	private String brdAmt;
	
	@JsonProperty("brdResp")
	private String brdResp;
	
	@JsonProperty("brdExpDate")
	private String brdExpDate;

	private String tokennumber;

	private String rsType;
	private BrdResponseArray[] rsBrdArray;
	private String rsOrderNumber;
	private String rsStoreNbr;
	private String rsNumberOfEntries;

	private JSONArray responseJsonArray;
}