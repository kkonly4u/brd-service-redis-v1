package com.belk.brd.model;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.index.Indexed;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "brdnumber", "tokennumber", "attributes" })
@Component
@AllArgsConstructor @NoArgsConstructor @Getter @Setter @ToString
public class Brd implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@NonNull @JsonProperty("brdnumber")
	private String brdnumber;

	@Id @Indexed @JsonProperty("tokennumber")
	private String tokennumber;

	@JsonProperty("attributes")
	private Attributes attributes;
}