package com.belk.brd.model;

import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;


@Component
@Getter @Setter
public class BrdResponseArray
{
    private String brdNbr;
}